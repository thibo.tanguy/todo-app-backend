# Todo app backend

API CRUD simple pour gérer des tâches avec les fonctionnalités de création, mise à jour, suppression, récupération de tâches individuelles et récupération de toutes les tâches.

### Installation Classique

Pour exécuter l'application en utilisant une installation classique, assurez-vous d'avoir Go installé sur votre machine. Voici les étapes :

#### 1. Se rendre dans le dossier avec les fichiers de l'application

```bash
cd src/app
```

#### 2. Installez les dépendances

```bash
go mod tidy
```

#### 3. Compilez et exécutez l'application

```bash
go run main.go
```

### Installation avec Docker

#### 1. Build l'image Docker

```bash
docker build -t todo-app-backend .
```

####  2. Run le conteneur Docker

```bash
docker run --name todo-app-backend -p 8080:8080 -d todo-app-backend
```

L'API devrait désormais être disponible à l'url http://localhost:8080

---

## Documentation de l'API

### Sommaire

```
- Get all tasks
- Get task by id
- Save task
- Update task
- Delete task
```

---

### Get all tasks

- **Endpoint** : GET /tasks
- **Description** : Récupérer toutes les tâches.
- **Exemple de réponse de la requête** :

```json
[
    {
      "id": 1,
      "title": "Tâche 1",
      "description": "Description de la tâche 1",
      "completed": false
    },
    {
      "id": 2,
      "title": "Tâche 2",
      "description": "Description de la tâche 2",
      "completed": false
    },
    {
      "id": 3,
      "title": "Tâche 3",
      "description": "Description de la tâche 3",
      "completed": false
    }
]
```

### Get task by id

- **Endpoint** : GET /tasks/{id}
- **Description** : Récupérer une tâche par son id.
- **Exemple de réponse de la requête** :

```json
{
  "title": "Tâche 1",
  "description": "Description de la tâche 1",
  "completed": false
}
```

### Save task

- **Endpoint** : POST /tasks
- **Description** : Crée une nouvelle tâche.
- **Exemple de corps de la requête** :

```json
{
  "title": "Tâche 1",
  "description": "Description de la tâche 1",
  "completed": false
}
```

### Update task

- **Endpoint** : PUT /tasks
- **Description** : Modifier une tâche.
- **Exemple de corps de la requête** :

```json
{
    "title": "Tâche 1",
    "description": "Description de la tâche 1",
    "completed": false
}
```

### Delete task

- **Endpoint** : DELETE /tasks/{id}
- **Description** : Supprimer une tâche par son id.
- **Exemple de réponse de la requête** :

```json
{
  "title": "Tâche 1",
  "description": "Description de la tâche 1",
  "completed": false
}
```