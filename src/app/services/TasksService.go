package services

import (
	"app/models"
	"app/repositories"
)

type TasksService struct {
	TasksRepository *repositories.TasksRepository
}

func NewTasksService(tasksRepository *repositories.TasksRepository) *TasksService {
	return &TasksService{
		TasksRepository: tasksRepository,
	}
}

func (s *TasksService) CreateTask(task *models.Task) error {
	return s.TasksRepository.Create(task)
}

func (s *TasksService) UpdateTask(task *models.Task) error {
	return s.TasksRepository.Update(task)
}

func (s *TasksService) DeleteTask(id uint) error {
	return s.TasksRepository.Delete(id)
}

func (s *TasksService) GetTaskByID(id uint) (*models.Task, error) {
	return s.TasksRepository.FindByID(id)
}

func (s *TasksService) GetAllTasks() ([]models.Task, error) {
	return s.TasksRepository.FindAll()
}
