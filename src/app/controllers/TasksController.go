package controllers

import (
	"app/models"
	"app/services"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type TasksController struct {
	TasksService *services.TasksService
}

func NewTasksController(taskService *services.TasksService) *TasksController {
	return &TasksController{
		TasksService: taskService,
	}
}

func (c *TasksController) CreateTask(ctx *gin.Context) {
	var task models.Task
	if err := ctx.ShouldBindJSON(&task); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := c.TasksService.CreateTask(&task); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Erreur lors de la création de la tâche"})
		return
	}
	ctx.JSON(http.StatusOK, task)
}

func (c *TasksController) UpdateTask(ctx *gin.Context) {
	var task models.Task
	if err := ctx.ShouldBindJSON(&task); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Requête JSON invalide"})
		return
	}

	taskId, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Id de tâche invalide"})
		return
	}

	task.Id = uint(taskId)
	if err := c.TasksService.UpdateTask(&task); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Erreur lors de la mise à jour de la tâche"})
		return
	}

	ctx.JSON(http.StatusOK, task)
}

func (c *TasksController) DeleteTask(ctx *gin.Context) {
	taskID, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Id de tâche invalide"})
		return
	}

	if err := c.TasksService.DeleteTask(uint(taskID)); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Erreur lors de la suppression de la tâche"})
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (c *TasksController) GetTaskByID(ctx *gin.Context) {
	taskID, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Id de tâche invalide"})
		return
	}

	task, err := c.TasksService.GetTaskByID(uint(taskID))
	if err != nil {
		ctx.JSON(http.StatusNotFound, gin.H{"error": "Tâche non trouvée"})
		return
	}

	ctx.JSON(http.StatusOK, task)
}

func (c *TasksController) GetAllTasks(ctx *gin.Context) {
	tasks, err := c.TasksService.GetAllTasks()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Erreur lors de la récupération des tâches"})
		return
	}

	ctx.JSON(http.StatusOK, tasks)
}
