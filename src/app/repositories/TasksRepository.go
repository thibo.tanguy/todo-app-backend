package repositories

import (
	"app/models"
	"gorm.io/gorm"
)

type TasksRepository struct {
	DB *gorm.DB
}

func NewTasksRepository(db *gorm.DB) *TasksRepository {
	return &TasksRepository{
		DB: db,
	}
}

func (r *TasksRepository) Create(task *models.Task) error {
	return r.DB.Create(task).Error
}

func (r *TasksRepository) Update(task *models.Task) error {
	return r.DB.Save(task).Error
}

func (r *TasksRepository) Delete(id uint) error {
	return r.DB.Delete(&models.Task{}, id).Error
}

func (r *TasksRepository) FindByID(id uint) (*models.Task, error) {
	var task models.Task
	err := r.DB.First(&task, id).Error
	if err != nil {
		return nil, err
	}
	return &task, nil
}

func (r *TasksRepository) FindAll() ([]models.Task, error) {
	var tasks []models.Task
	err := r.DB.Find(&tasks).Error
	if err != nil {
		return nil, err
	}
	return tasks, nil
}
