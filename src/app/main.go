package main

import (
	"app/controllers"
	"app/repositories"
	"app/routes"
	"app/services"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	// Configuration de la base de données
	db, err := gorm.Open(mysql.Open("root:test@tcp(database:3306)/todo_app?charset=utf8&parseTime=True&loc=Local"), &gorm.Config{})
	if err != nil {
		panic("Impossible de se connecter à la base de données")
	}

	// Création des instances de services, controllers, repositories
	tasksRepository := repositories.NewTasksRepository(db)
	tasksService := services.NewTasksService(tasksRepository)
	tasksController := controllers.NewTasksController(tasksService)

	// Définition des routes API
	r := gin.Default()
	routes.SetupRoutes(r, tasksController)

	// Lancement du serveur
	err = r.Run(":8080")
	if err != nil {
		return
	}
}
