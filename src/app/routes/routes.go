package routes

import (
	"app/controllers"
	"github.com/gin-gonic/gin"
)

func SetupRoutes(router *gin.Engine, tasksController *controllers.TasksController) {
	router.POST("/tasks", tasksController.CreateTask)
	router.PUT("/tasks/:id", tasksController.UpdateTask)
	router.DELETE("/tasks/:id", tasksController.DeleteTask)
	router.GET("/tasks/:id", tasksController.GetTaskByID)
	router.GET("/tasks", tasksController.GetAllTasks)
}
